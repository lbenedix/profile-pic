#!/bin/bash

if [ -z "$1" ]; then
    echo "Usage: ./install nick"
    exit 1
fi


url="https://chaos.social/@$1"

echo "URL: $url"

image_url=$(curl -s $url | grep "avatar" | grep -o 'https://[^"]*' | cut -d '"' -f 1)

echo "Image URL: $image_url"

curl -s $image_url | convert - -resize 240x240 -transparent black profile.png

mpremote fs mkdir :/flash/sys/apps/profile-pic
mpremote fs cp flow3r.toml :/flash/sys/apps/profile-pic/flow3r.toml
mpremote fs cp __init__.py :/flash/sys/apps/profile-pic/__init__.py
mpremote fs cp profile.png :/flash/profile.png