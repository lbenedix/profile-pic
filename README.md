# how to install

```bash
# convert image to the right format
convert your_image.jpeg -resize 240x240 -transparent black profile.png

# upload files to flow3r
mpremote fs mkdir :/flash/sys/apps/profile-pic
mpremote fs cp flow3r.toml :/flash/sys/apps/profile-pic/flow3r.toml
mpremote fs cp __init__.py :/flash/sys/apps/profile-pic/__init__.py
mpremote fs cp profile.png :/flash/profile.png
```

# set your chaos.social profile pic

```bash
./install your_nick
```